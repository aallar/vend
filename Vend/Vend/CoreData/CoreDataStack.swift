//
//  CoreDataStack.swift
//  Vend
//
//  Created by allar on 10/07/21.
//

import UIKit
import CoreData

open class CoreDataStack {
    static let modelName = "Vend"

    lazy var container: NSPersistentContainer = {
        let container = NSPersistentContainer(name: CoreDataStack.modelName)
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
            container.viewContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        })
        return container
    }()
        
    var context: NSManagedObjectContext {
        return container.viewContext
    }
    
    func saveContext() {
        guard context.hasChanges else {
            return
        }
        
        do {
            try context.save()
        } catch let error {
            print("Unresolved error \(error)")
        }
    }
}
