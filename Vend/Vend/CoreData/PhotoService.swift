//
//  PhotoService.swift
//  Vend
//
//  Created by allar on 10/07/21.
//

import CoreData

final class PhotoService {
    let coreDataStack: CoreDataStack
    let apiClient: APIClientProtocol

    init(coreDataStack: CoreDataStack, apiClient: APIClientProtocol = APIClient()) {
        self.coreDataStack = coreDataStack
        self.apiClient = apiClient
    }
    
    @discardableResult
    func addPhoto(id: String, downloadUrl: String, author: String) -> Photo {
        let photo = Photo(context: coreDataStack.context)
        photo.id = id
        photo.downloadUrl = downloadUrl
        photo.author = author
        photo.createdAt = Date()
        
        coreDataStack.saveContext()
        return photo
    }
    
    func getRandomPhoto(completion: @escaping ((Result<PhotoAPIModel?, Error>) -> Void)) {
        apiClient.getPhotos { result in
            switch result {
            case .success(let photos):
                completion(.success(photos.randomElement()))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func delete(photo: Photo) {
        coreDataStack.context.delete(photo)
        coreDataStack.saveContext()
    }
}
