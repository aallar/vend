//
//  APIRouter.swift
//  Vend
//
//  Created by allar on 10/07/21.
//

import Foundation
import Alamofire

enum APIRouter: URLRequestConvertible {
    case getPhotos
    
    private var baseURL: URL {
        return URL(string: "https://picsum.photos/v2")!
    }
    private var method: HTTPMethod {
        switch self {
        case .getPhotos:
            return .get
        }
    }
    private var path: String {
        switch self {
        case .getPhotos:
            return "list"
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = baseURL.appendingPathComponent(path)
        var request = URLRequest(url: url)
        request.method = method
        return request
    }
}
