//
//  APIClient.swift
//  Vend
//
//  Created by allar on 10/07/21.
//

import Alamofire
import Foundation

protocol APIClientProtocol {
    func getPhotos(completion: @escaping ((Result<[PhotoAPIModel], Error>) -> Void))
}

final class APIClient: APIClientProtocol {
    private lazy var decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        return decoder
    }()

    func getPhotos(completion: @escaping ((Result<[PhotoAPIModel], Error>) -> Void)) {
        AF.request(APIRouter.getPhotos).responseDecodable(of: [PhotoAPIModel].self, decoder: decoder) { response in
            switch response.result {
            case .success(let photos):
                completion(.success(photos))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}

