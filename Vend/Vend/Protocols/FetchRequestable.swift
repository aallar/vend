//
//  FetchRequestable.swift
//  Vend
//
//  Created by allar on 10/07/21.
//

import CoreData

protocol FetchRequestable {
    static var entityName: String { get }
}

extension FetchRequestable where Self: NSManagedObject {
    static var entityName: String {
        return String(describing: self)
    }
    
    static var fetchRequest: NSFetchRequest<Self> {
        return NSFetchRequest<Self>(entityName: entityName)
    }
}
