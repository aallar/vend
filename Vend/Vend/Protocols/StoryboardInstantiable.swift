//
//  StoryboardInstantiable.swift
//  Vend
//
//  Created by allar on 10/07/21.
//

import UIKit

enum StoryboardName: String {
    case main = "Main"
}

protocol StoryboardInstantiable {
    static var identifier: String { get }
    static var storyboardName: StoryboardName { get }
    
    static func create() -> Self
}

extension StoryboardInstantiable where Self: UIViewController {
    static var storyboardName: StoryboardName {
        return .main
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    static func create() -> Self {
        guard let viewController = UIStoryboard(name: storyboardName.rawValue, bundle: nil).instantiateViewController(identifier: identifier) as? Self else {
            fatalError("View controller with \(identifier) not found")
        }
        return viewController
    }
}
