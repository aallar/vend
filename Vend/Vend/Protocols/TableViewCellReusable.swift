//
//  TableViewCellReusable.swift
//  Vend
//
//  Created by allar on 10/07/21.
//

import UIKit

protocol TableViewCellReusable {
    associatedtype ConfigurationItem
    
    static var reuseIdentifier: String { get }
    static func create(in tableView: UITableView, at indexPath: IndexPath) -> Self
    
    func configure(with configItem: ConfigurationItem)
}

extension TableViewCellReusable {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
    
    static func create(in tableView: UITableView, at indexPath: IndexPath) -> Self {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as? Self else {
            fatalError("Cell with \(reuseIdentifier) identifier not found")
        }
        return cell
    }
}
