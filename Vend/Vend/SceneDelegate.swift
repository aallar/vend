//
//  SceneDelegate.swift
//  Vend
//
//  Created by allar on 10/07/21.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        window = UIWindow(frame: windowScene.coordinateSpace.bounds)
        window?.windowScene = windowScene
        let photosViewController = PhotosTableViewController.create()
        window?.rootViewController = UINavigationController(rootViewController: photosViewController)
        window?.makeKeyAndVisible()
    }
}
