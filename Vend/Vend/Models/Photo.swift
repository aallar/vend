//
//  Photo.swift
//  Vend
//
//  Created by allar on 10/07/21.
//

import CoreData

final class Photo: NSManagedObject, Identifiable, FetchRequestable {
    @NSManaged var author: String
    @NSManaged var downloadUrl: String
    @NSManaged var id: String
    @NSManaged var createdAt: Date
}

extension Photo {
    static func listAllFetchRequest() -> NSFetchRequest<Photo> {
        let fetchRequest = Photo.fetchRequest
        let sortDescriptor = NSSortDescriptor(key: #keyPath(Photo.createdAt), ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        return fetchRequest
    }
}
