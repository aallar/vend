//
//  PhotoAPIModel.swift
//  Vend
//
//  Created by allar on 10/07/21.
//

import Foundation

struct PhotoAPIModel: Decodable {
    let id: String
    let author: String
    let downloadUrl: String
}
