//
//  PhotoTableViewCell.swift
//  Vend
//
//  Created by allar on 10/07/21.
//

import UIKit
import Kingfisher

final class PhotoTableViewCell: UITableViewCell, TableViewCellReusable {
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var photoImageView: UIImageView!
    
    func configure(with photo: Photo) {
        authorLabel.text = photo.author
        photoImageView.kf.setImage(with: URL(string: photo.downloadUrl))
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        photoImageView.kf.cancelDownloadTask()
    }
}

