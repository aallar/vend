//
//  PhotosTableViewController.swift
//  Vend
//
//  Created by allar on 10/07/21.
//

import UIKit
import CoreData

final class PhotosTableViewController: UITableViewController, StoryboardInstantiable {
    enum Section {
        case photos
    }
    private typealias Snapshot = NSDiffableDataSourceSnapshot<Section, Photo>
    
    private lazy var fetchedResultsController: NSFetchedResultsController<Photo> = {
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: Photo.listAllFetchRequest(), managedObjectContext: photoService.coreDataStack.context, sectionNameKeyPath: nil, cacheName: nil)
        fetchedResultsController.delegate = self
        return fetchedResultsController
    }()
    private lazy var dataSource = PhotosTableViewDiffableDataSource(tableView: tableView, cellProvider: { (tableView, indexPath, photo) -> UITableViewCell? in
        let cell = PhotoTableViewCell.create(in: tableView, at: indexPath)
        cell.configure(with: photo)
        return cell
    })
    
    private let photoService: PhotoService = PhotoService(coreDataStack: CoreDataStack())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.accessibilityIdentifier = "PhotosTableView"
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.rightBarButtonItem = editButtonItem
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Add", primaryAction: UIAction(handler: { _ in
            self.addRandomPhoto()
        }))
        
        try? fetchedResultsController.performFetch()
    }
    
    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { [weak self] _, _, completion in
            guard let self = self, let photo = self.dataSource.itemIdentifier(for: indexPath) else {
                return
            }
            self.photoService.delete(photo: photo)
            completion(true)
        }
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
    
    private func addRandomPhoto() {
        photoService.getRandomPhoto { [weak self] result in
            guard let self = self else {
                return
            }
            switch result {
            case .success(let photo):
                if let photo = photo {
                    self.photoService.addPhoto(id: photo.id, downloadUrl: photo.downloadUrl, author: photo.author)
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}

extension PhotosTableViewController: NSFetchedResultsControllerDelegate {
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChangeContentWith snapshot: NSDiffableDataSourceSnapshotReference) {
        var snapshot = NSDiffableDataSourceSnapshot<Section, Photo>()
        snapshot.appendSections([.photos])
        let photos = fetchedResultsController.fetchedObjects ?? []
        snapshot.appendItems(photos)
        dataSource.apply(snapshot, animatingDifferences: view.window != nil)
    }
}
