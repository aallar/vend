//
//  PhotosTableViewDiffableDataSource.swift
//  Vend
//
//  Created by allar on 10/07/21.
//

import UIKit

final class PhotosTableViewDiffableDataSource: UITableViewDiffableDataSource<PhotosTableViewController.Section, Photo> {
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
}
