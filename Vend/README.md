# README #
 
### Architecture ###
 
* The app uses MVC architecture.
* Networking is handled by the APIClient class which uses Alamofire dependency. 
* Persistence is managed by the CoreDataStack class.
* Adding and deleting photos is handled by the PhotoService class.
 
 
### UI ###
 
* The UI elements behave like good platform citizens.  They support dynamic type and dark mode.
* The UI is implemented by using a storyboard and autolayout.
* PhotosTableViewController uses NSFetchedResultsController which updates UITableViewDiffableDataSource ensuring UI stays up to date.
 
 
### Tests ###
 
* There are some PhotoService unit tests covering  adding and deleting a photo. Unit tests use TestCoreDataStack which stores its data in memory (NSInMemoryStoreType).
* UI tests cover functionality of adding a photo, deleting a photo and toggling table view editing mode.
 
 
### External dependencies ###

* [Alamofire](https://github.com/Alamofire/Alamofire) - Used for networking.
* [Kingfisher](https://github.com/onevcat/Kingfisher) - Used for downloading and displaying images.
 
 
### TODO ###

* Add functionality to reorder items in a table view.
* Improve test coverage.



