//
//  VendUITests.swift
//  VendUITests
//
//  Created by allar on 10/07/21.
//

import XCTest

class VendUITests: XCTestCase {
    func testAddPhoto() throws {
        let app = XCUIApplication()
        app.launch()
     
        let tableView = app.tables["PhotosTableView"]
        XCTAssertTrue(tableView.exists, "The tableview exists")
        
        let previousCellCount = tableView.cells.count
        app.navigationBars.buttons["Add"].tap()
        
        let cell = tableView.cells.firstMatch.waitForExistence(timeout: 10)
        XCTAssertNotNil(cell, "cell should not be nil")

        let currentCellCount = tableView.cells.count
     
        XCTAssertGreaterThan(currentCellCount, previousCellCount)
    }
    
    func testEditingMode() throws {
        let app = XCUIApplication()
        app.launch()
        app.navigationBars.buttons["Edit"].tap()
        app.navigationBars.buttons["Done"].tap()
    }
    
    func testDeletePhoto() throws {
        let app = XCUIApplication()
        app.launch()

        let tableView = app.tables["PhotosTableView"]
        XCTAssertTrue(tableView.exists, "The tableview exists")
        
        let tablesCellQuery = app.tables.cells
        
        let previousCellCount = tablesCellQuery.count
        app.navigationBars.buttons["Add"].tap()
        
        let cell = tableView.cells.firstMatch.waitForExistence(timeout: 10)
        XCTAssertNotNil(cell, "cell should not be nil")

        tablesCellQuery.element(boundBy: 0).swipeLeft(velocity: .slow)
        tablesCellQuery.element(boundBy: 0).buttons["Delete"].tap()
        
        let currentCellCount = tablesCellQuery.count
        
        XCTAssertEqual(currentCellCount, previousCellCount)
    }
}
