//
//  TestAPIClient.swift
//  VendTests
//
//  Created by allar on 10/07/21.
//

import Foundation
@testable import Vend

final class TestAPIClient: APIClientProtocol {
    func getPhotos(completion: @escaping ((Result<[PhotoAPIModel], Error>) -> Void)) {
        let mockedPhotos = [PhotoAPIModel(id: "1", author: "Alejandro Escamilla", downloadUrl: "https://picsum.photos/id/0/5616/3744"),
                            PhotoAPIModel(id: "1025", author: "William Hook", downloadUrl: "https://picsum.photos/id/1025/4951/3301")]
        completion(.success(mockedPhotos))
    }
}

