//
//  PhotoServiceTests.swift
//  VendTests
//
//  Created by allar on 10/07/21.
//

import XCTest
import CoreData
@testable import Vend

final class PhotoServiceTests: XCTestCase {
    var photoService: PhotoService!
    
    override func setUp() {
        super.setUp()
        photoService = PhotoService(coreDataStack: TestCoreDataStack(), apiClient: TestAPIClient())
    }
    
    override func tearDown() {
        super.tearDown()
        photoService = nil
    }
    
    func testAddPhoto() {
        let photo = photoService.addPhoto(id: "1", downloadUrl: "https://google.com", author: "Steve Smith")
        
        XCTAssertNotNil(photo, "Photo should not be nil")
        XCTAssertTrue(photo.id == "1")
        XCTAssertTrue(photo.downloadUrl == "https://google.com")
        XCTAssertTrue(photo.author == "Steve Smith")
        XCTAssertNotNil(photo.createdAt, "createdAt should not be nil")
    }
    
    func testGetRandomPhoto() {
        let promise = expectation(description: "getting random photo")
        var randomPhoto: PhotoAPIModel?
        photoService.getRandomPhoto { result in
            if case let .success(photo) = result {
                randomPhoto = photo
            }
            promise.fulfill()
        }
        waitForExpectations(timeout: 1, handler: nil)
        XCTAssertNotNil(randomPhoto, "random photo should not be nil")
    }
    
    func testDeletePhoto() {
        let photo = photoService.addPhoto(id: "33", downloadUrl: "https://google.com", author: "Tim Apple")
        
        let photosCountAfterAdd = try! photoService.coreDataStack.context.count(for: Photo.fetchRequest)
        XCTAssertTrue(photosCountAfterAdd == 1)
        
        photoService.delete(photo: photo)
        let photosCountAfterDelete = try! photoService.coreDataStack.context.count(for: Photo.fetchRequest)
        XCTAssertTrue(photosCountAfterDelete == 0)
    }
}
